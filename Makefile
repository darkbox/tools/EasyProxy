
none:
	echo None.

up:
	docker compose up --build easyproxy;

up_no_build:
	docker compose up easyproxy;

run:
	docker compose build easyproxy_bash && docker compose run easyproxy_bash;

build_and_up:
	docker compose build easyproxy && docker compose push easyproxy;